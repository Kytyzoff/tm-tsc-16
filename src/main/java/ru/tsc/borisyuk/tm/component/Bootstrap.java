package ru.tsc.borisyuk.tm.component;

import ru.tsc.borisyuk.tm.api.controller.ICommandController;
import ru.tsc.borisyuk.tm.api.controller.IProjectController;
import ru.tsc.borisyuk.tm.api.controller.IProjectTaskController;
import ru.tsc.borisyuk.tm.api.controller.ITaskController;
import ru.tsc.borisyuk.tm.api.repository.ICommandRepository;
import ru.tsc.borisyuk.tm.api.repository.IProjectRepository;
import ru.tsc.borisyuk.tm.api.repository.ITaskRepository;
import ru.tsc.borisyuk.tm.api.service.*;
import ru.tsc.borisyuk.tm.constant.ArgumentConst;
import ru.tsc.borisyuk.tm.constant.TerminalConst;
import ru.tsc.borisyuk.tm.controller.CommandController;
import ru.tsc.borisyuk.tm.controller.ProjectController;
import ru.tsc.borisyuk.tm.controller.ProjectTaskController;
import ru.tsc.borisyuk.tm.controller.TaskController;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.exception.system.UnknownCommandException;
import ru.tsc.borisyuk.tm.model.Project;
import ru.tsc.borisyuk.tm.model.Task;
import ru.tsc.borisyuk.tm.repository.CommandRepository;
import ru.tsc.borisyuk.tm.repository.ProjectRepository;
import ru.tsc.borisyuk.tm.repository.TaskRepository;
import ru.tsc.borisyuk.tm.service.*;

import java.util.Date;
import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService, taskService, projectService);

    private final ILogService logService = new LogService();

    public void start(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        initData();
        parseArgs(args);
        logService.debug("Test environment.");
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = scanner.nextLine();
                logService.command(command);
                parseCommand(command);
                logService.info("Completed");
            } catch (final Exception e) {
                logService.error(e);
            }
        }
    }

    private void initData() {
        projectService.add(new Project("Project 1", "1", new Date(2003,01,01)));
        projectService.changeStatusByName("Project 1", Status.COMPLETED);
        projectService.add(new Project("Project 2", "2", new Date(2002,01,01)));
        projectService.add(new Project("Project 3", "3", new Date(2001,01,01)));
        projectService.changeStatusByName("Project 3", Status.IN_PROGRESS);
        taskService.add(new Task("Task a", "1", new Date(03,01,01)));
        taskService.changeStatusByName("Task a", Status.COMPLETED);
        taskService.add(new Task("Task b", "2", new Date(02,01,01)));
        taskService.add(new Task("Task c", "3", new Date(01,01,01)));
        taskService.changeStatusByName("Task c", Status.IN_PROGRESS);
    }

    public void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }


    public void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    public void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showById();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showByIndex();
                break;
            case TerminalConst.TASK_SHOW_BY_NAME:
                taskController.showByName();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeByName();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startByIndex();
                break;
            case TerminalConst.TASK_START_BY_NAME:
                taskController.startByName();
                break;
            case TerminalConst.TASK_FINISH_BY_ID:
                taskController.finishById();
                break;
            case TerminalConst.TASK_FINISH_BY_INDEX:
                taskController.finishByIndex();
                break;
            case TerminalConst.TASK_FINISH_BY_NAME:
                taskController.finishByName();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeStatusByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_NAME:
                taskController.changeStatusByName();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showById();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showByIndex();
                break;
            case TerminalConst.PROJECT_SHOW_BY_NAME:
                projectController.showByName();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeByName();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_NAME:
                projectController.startByName();
                break;
            case TerminalConst.PROJECT_FINISH_BY_ID:
                projectController.finishById();
                break;
            case TerminalConst.PROJECT_FINISH_BY_INDEX:
                projectController.finishByIndex();
                break;
            case TerminalConst.PROJECT_FINISH_BY_NAME:
                projectController.finishByName();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeStatusByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_NAME:
                projectController.changeStatusByName();
                break;
            case TerminalConst.TASKS_SHOW_BY_PROJECT_ID:
                projectTaskController.findAllTaskByProjectId();
                break;
            case TerminalConst.BIND_TASK_TO_PROJECT_BY_ID:
                projectTaskController.bindTaskToProjectById();
                break;
            case TerminalConst.UNBIND_TASK_FROM_PROJECT_BY_ID:
                projectTaskController.unbindTaskFromProjectById();
                break;
            case TerminalConst.TASKS_REMOVE_FROM_PROJECT_BY_ID:
                projectTaskController.removeAllTaskByProjectId();
                break;
            case TerminalConst.PROJECT_REMOVE_WITH_TASKS_BY_ID:
                projectTaskController.removeProjectById();
                break;
            default:
                throw new UnknownCommandException();
        }
    }

}
