package ru.tsc.borisyuk.tm.controller;

import ru.tsc.borisyuk.tm.api.controller.IProjectTaskController;
import ru.tsc.borisyuk.tm.api.service.IProjectService;
import ru.tsc.borisyuk.tm.api.service.IProjectTaskService;
import ru.tsc.borisyuk.tm.api.service.ITaskService;
import ru.tsc.borisyuk.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.borisyuk.tm.exception.entity.TaskNotFoundException;
import ru.tsc.borisyuk.tm.model.Task;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private IProjectTaskService projectTaskService;

    private ITaskService taskService;

    private IProjectService projectService;

    public ProjectTaskController(IProjectTaskService projectTaskService, ITaskService taskService, IProjectService projectService) {
        this.projectTaskService = projectTaskService;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Override
    public void bindTaskToProjectById() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (projectService.findById(projectId) == null) throw new ProjectNotFoundException();
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        if (taskService.findById(taskId) == null) throw new TaskNotFoundException();
        final Task taskUpdated = projectTaskService.bindTaskById(projectId, taskId);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

    @Override
    public void unbindTaskFromProjectById() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (projectService.findById(projectId) == null) throw new ProjectNotFoundException();
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        if (taskService.findById(taskId) == null) throw new TaskNotFoundException();
        projectTaskService.unbindTaskById(projectId, taskId);
    }

    @Override
    public void removeAllTaskByProjectId() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (projectService.findById(projectId) == null) throw new ProjectNotFoundException();
        projectTaskService.removeAllTaskByProjectId(projectId);
    }

    @Override
    public void removeProjectById() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (projectService.findById(projectId) == null) throw new ProjectNotFoundException();
        projectTaskService.removeById(projectId);
    }

    @Override
    public void findAllTaskByProjectId() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (projectService.findById(projectId) == null) throw new ProjectNotFoundException();
        System.out.println(projectTaskService.findAllTaskByProjectId(projectId));
    }
}
