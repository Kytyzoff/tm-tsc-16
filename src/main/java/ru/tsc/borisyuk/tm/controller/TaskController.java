package ru.tsc.borisyuk.tm.controller;

import ru.tsc.borisyuk.tm.api.controller.ITaskController;
import ru.tsc.borisyuk.tm.api.service.IProjectService;
import ru.tsc.borisyuk.tm.api.service.ITaskService;
import ru.tsc.borisyuk.tm.enumerated.Sort;
import ru.tsc.borisyuk.tm.enumerated.Status;
import ru.tsc.borisyuk.tm.exception.entity.TaskNotFoundException;
import ru.tsc.borisyuk.tm.model.Task;
import ru.tsc.borisyuk.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private ITaskService taskService;

    private IProjectService projectService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void showTasks() {
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        final List<Task> tasks;
        System.out.println("[LIST TASKS]");
        if (sort == null || sort.isEmpty()) tasks = taskService.findAll();
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = taskService.findAll(sortType.getComparator());
        }
        for (Task task : tasks)
        { System.out.println(tasks.indexOf(task) + 1 + ". " + task.toString() + ". " + task.getStatus() + ". " + task.getStartDate()); }
        System.out.println("[OK]");
    }

    @Override
    public void showTask(Task task) {
        if (task == null) return;
        final List<Task> tasks = taskService.findAll();
        final Integer indexNum = tasks.indexOf(task) + 1;
        System.out.println("Index: " + indexNum);
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Project id: " + task.getProjectId());
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findById(id);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findByName(name);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void removeById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        System.out.println("[REMOVE TASK BY ID]");
        taskService.removeById(id);
        System.out.println("[OK]");
    }

    @Override
    public void removeByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[REMOVE TASK BY INDEX]");
        taskService.removeByIndex(index);
        System.out.println("[OK]");

    }

    @Override
    public void removeByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("[REMOVE TASK BY NAME]");
        taskService.removeByName(name);
        System.out.println("[OK]");
    }

    @Override
    public void updateById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        if (!taskService.existsById(id)) throw new TaskNotFoundException();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateById(id, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

    @Override
    public void updateByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!taskService.existsByIndex(index)) throw new TaskNotFoundException();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateByIndex(index, name, description);
        if (taskUpdated == null) throw new TaskNotFoundException();
    }

    @Override
    public void startById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void startByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.startByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void startByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.finishById(id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.finishByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void finishByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeStatusById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusById(id, status);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusByIndex(index, status);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeStatusByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusByName(name, status);
        if (task == null) throw new TaskNotFoundException();
    }

}
